import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
 
@Injectable()
export class Reviews {
 
  data: any;
 
  constructor(public http: Http) {
    this.data = null;
  }
 
  getReviews(){
 
    if (this.data) {
      return Promise.resolve(this.data);
    }
 
    return new Promise(resolve => {
 
      this.http.get('http://18.188.64.127:8080/api/reviews')
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });
 
  }

 
 
  createReview(review){
 
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
 
    this.http.post('http://18.188.64.127:8080/api/reviews', JSON.stringify(review), {headers: headers})
      .subscribe(res => {
        console.log(res.json());
      });
 
  }
 
  deleteReview(id){
 
    this.http.delete('http://18.188.64.127:8080/api/reviews/' + id).subscribe((res) => {
      console.log(res.json());
    });   
 
  }
 
  getDevelopers(){
 
    if (this.data) {
      return Promise.resolve(this.data);
    }
 
    return new Promise(resolve => {
 
      this.http.get('http://18.188.64.127:8080/api/developers')
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });
 
  }
 
  createDeveloper(developer){
 
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
 
    this.http.post('http://18.188.64.127:8080/api/developers', JSON.stringify(developer), {headers: headers})
      .subscribe(res => {
        console.log(res.json());
      });
 
  }
 
  deleteDeveloper(id){
 
    this.http.delete('http://18.188.64.127:8080/api/developers/' + id).subscribe((res) => {
      console.log(res.json());
    });   
 
  }
}