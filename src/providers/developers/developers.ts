import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

/*
  Generated class for the DevelopersProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class Developers {
  data: any;
 
  constructor(public http: HttpClient) {
    this.data = null;
  }
 
  getReviews(){
 
    if (this.data) {
      return Promise.resolve(this.data);
    }
 
    return new Promise(resolve => {
 
      this.http.get('http://18.188.64.127:8080/api/reviews')
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });
 
  }

 
 
  createReview(review){
 
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
 
    this.http.post('http://18.188.64.127:8080/api/reviews', JSON.stringify(review), {headers: headers})
      .subscribe(res => {
        console.log(res);
      });
 
  }
 
  deleteReview(id){
 
    this.http.delete('http://18.188.64.127:8080/api/reviews/' + id).subscribe((res) => {
      console.log(res);
    });   
 
  }
 
  getDevelopers(){
 
    if (this.data) {
      return Promise.resolve(this.data);
    }
 
    return new Promise(resolve => {
 
      this.http.get('http://18.188.64.127:8080/api/developers')
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });
 
  }
 
  createDeveloper(developer){
 
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
 
    this.http.post('http://18.188.64.127:8080/api/developers', JSON.stringify(developer), {headers: headers})
      .subscribe(res => {
        console.log(res);
      });
 
  }
 
  deleteDeveloper(id){
 
    this.http.delete('http://18.188.64.127:8080/api/developers/' + id).subscribe((res) => {
      console.log(res);
    });   
 
  }
}