import { Component } from "@angular/core";
import { NavController, ModalController, IonicPage } from 'ionic-angular';
import { Reviews } from '../../providers/reviews/reviews';

/**
 * Generated class for the DevelopersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-developers',
  templateUrl: 'developers.html',
})
export class DevelopersPage {

  developers: any;
 
  constructor(public nav: NavController, public reviewService: Reviews, public modalCtrl: ModalController) {
 
  }
 
  ionViewDidLoad(){
 
    this.reviewService.getDevelopers().then((data) => {
      console.log(data);
      this.developers = data;
    });
 
  }
 
  addDeveloper(){
 
    let modal = this.modalCtrl.create('AddDeveloperPage');
 
    modal.onDidDismiss(developer => {
      if(developer){
        this.developers.push(developer);
        this.reviewService.createDeveloper(developer);       
      }
    });
 
    modal.present();
 
  }
 
  deleteDeveloper(developer){
 
    //Remove locally
      let index = this.developers.indexOf(developer);
 
      if(index > -1){
        this.developers.splice(index, 1);
      }  
 
    //Remove from database
    this.reviewService.deleteDeveloper(developer._id);
  }
 
}