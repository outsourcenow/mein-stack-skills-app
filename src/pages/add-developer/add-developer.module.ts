import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddDeveloperPage } from './add-developer';

@NgModule({
  declarations: [
    AddDeveloperPage,
  ],
  imports: [
    IonicPageModule.forChild(AddDeveloperPage),
  ],
})
export class AddDeveloperPageModule {}
