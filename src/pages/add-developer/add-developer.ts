import { Component } from '@angular/core';
import { ViewController, IonicPage } from 'ionic-angular';
/**
 * Generated class for the AddDeveloperPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-developer',
  templateUrl: 'add-developer.html',
})
export class AddDeveloperPage {
  name: string;
  age: number;
  languages: any;
  rating: any;
 
  constructor(public viewCtrl: ViewController) {
 
  }
 
  save(): void {
 
    let review = {
      name: this.name,
      age: this.age,
      languages: this.languages,
      rating: this.rating
    };
 
    this.viewCtrl.dismiss(review);
 
  }
 
  close(): void {
    this.viewCtrl.dismiss();
  }
}