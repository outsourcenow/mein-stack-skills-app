#The DevC Skills App
An Ionic3 MongoDb and Node Application, Inspired by the Lusaka Facebook Developer Circles Meetup
You can Find the original repo here https://github.com/DeveloperCirclesLusaka/MERN-STACK

I decided to rewrite the front end with the ionic framework i love so much, I guess we could now call this the MEIN STACK

New to Ionic? or You have never heard of it? No worries I am here to help, Its as easy as a bottle of castle light down your throat!!

I will take it you are new to the framework! So lets dive in.

##Installation of Ionic 
1. 	Download and install the latest LTS NodeJS from the following link:

-	 https://nodejs.org/en/

2. 	Install ionic and Cordova with:

	```
	npm install -g ionic cordova
	```

3. 	You will need mongoDb, for windows, head over to:

-	https://www.mongodb.com/dr/fastdl.mongodb.org/win32/mongodb-win32-x86_64-2008plus-ssl-4.0.1-signed.msi/download

	Install it as a service and you are good, this tutorial uses the default configuration.
	
4.	We will need our backend! to be in full control you will need this setup locally, But by default its already pointing to my remote backend so
	you can test as soon as you are done cloning the repo. TO Setup your own backend you can clone:

	```
	git clone https://Mukopaje-Micko@bitbucket.org/outsourcenow/devc-server.git
	```
	
	
	```
	cd devc-server
	```
	
	```
	npm install
	```

	
	```
	node server.js
	```
	
Please make sure that mongoDb in installed and running on the default port.
and if you are using you local server go to the project and edit the url in /src/providers/developers/developers.ts 
from http://18.188.64.127:8080/api/developers/ to http://localhost:8080/api/developers/
to point to your local server.


5. 	We almost there! you don’t need Mosi or castle Light, Coffee will be fine!!
	If all the dependencies are installed then Just clone the repo
	
	```
	git clone https://Mukopaje-Micko@bitbucket.org/outsourcenow/mein-stack-skills-app.git
	```
	
	```
	cd mein-stack-skills-app
	```
	
	```
	ionic serve
	```
	
If all went well you will see you browser spin up and tell you that you just won a Thousand dollars on port 8100!
	
## Cordova
		
Cordova in this case will allow us to build native apps, But this should be a topic for another day, So you have to wait.
Wait a minute i love you! so if you cant wait and would like to build an apk after you try out your changes you could read the article bellow to help you set 
up you environment:

https://medium.com/appseed-io/how-to-prepare-your-windows-box-for-ionic-3-development-f1e32ec8380e
	
